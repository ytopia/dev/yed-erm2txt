# yed-erm2txt

## Requirements
- node
- npm

## Installation

```bash
curl https://gitlab.com/youtopia.earth/dev/yed-erm2txt/raw/master/install | bash
```

## Usage

```bash
yed-erm2txt <file_input> [file_output]
```

## License
[MIT](https://choosealicense.com/licenses/mit/)